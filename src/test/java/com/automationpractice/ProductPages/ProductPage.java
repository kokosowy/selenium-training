package com.automationpractice.ProductPages;

import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Asserts;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static com.automationpractice.commonclasses.Waits.*;

@Slf4j
public class ProductPage {

    @FindBy(css = "iframe[class='fancybox-iframe']")
    private WebElement productFrame;

    @FindBy(css = "div[class='pb-center-column col-xs-12 col-sm-4']>h1")
    private WebElement productTitleLabel;

    @FindBy(id = "color_14")
    private WebElement changeColorButton;

    @FindBy(id = "wishlist_button")
    private WebElement addToWishlistButton;

    public ProductPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(Driver.getInstance(), this);
        Driver.manageImplicitWait();
    }

    public ProductPage switchToProductFrame() {
        log.info("Switching to current frame.");
        Driver.switchToFrame(productFrame);
        return this;
    }

    public ProductPage waitUntilProductLabelIsDisplayed() {
        log.info("Waiting until product label is visible.");
        waitUntilVisibilityOf(productTitleLabel);
        return this;
    }

    public ProductPage assertProductPageIsDisplayed() {
        log.info("Checking if product page is displayed with required elements.");
        Asserts.assertIsDisplayed(productTitleLabel,
                changeColorButton,
                addToWishlistButton);
        return this;
    }

    public ProductPage changeColor() {
        log.info("Changing color of the product.");
        changeColorButton.click();
        return this;
    }

    public ConfirmWishlistPage addToWishlist() {
        log.info("Adding to product to wishlist.");
        addToWishlistButton.click();
        return new ConfirmWishlistPage();
    }

    public ErrorWhishlistPage addToWishlistWithoutLoggingIn() {
        log.info("Adding to product to wishlist.");
        addToWishlistButton.click();
        return new ErrorWhishlistPage();
    }

}
