package com.automationpractice.ProductPages;

import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Asserts;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static helper.PathsAndTexts.*;

@Slf4j
public class ConfirmWishlistPage {

    //@FindBy(css = "[class='fancybox-wrap fancybox-desktop fancybox-type-html fancybox-opened']")
    @FindBy(xpath = "//*[@id='product']/div[2]/div/div/div/div/p")
    private WebElement confirmationPopUp;

    @FindBy(css = "p[class='fancybox-error']")
    private WebElement confirmationMessage;

    public ConfirmWishlistPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(Driver.getInstance(), this);
        Driver.manageImplicitWait();
    }

    public ConfirmWishlistPage assertConfirmationIsDisplayed() {
        log.info("Checking if confirmation message about adding product to wishlist is displayed properly.");
        Asserts.assertIsDisplayed(confirmationPopUp);
        Asserts.assertMessageIsCorrect(confirmationMessage, CONFIRMWISHLIST_MESSAGE_SUCCESS);
        return this;
    }

}
