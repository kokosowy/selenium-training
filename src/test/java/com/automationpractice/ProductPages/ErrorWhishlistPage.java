package com.automationpractice.ProductPages;

import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Asserts;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static helper.PathsAndTexts.*;

@Slf4j
public class ErrorWhishlistPage {

    @FindBy (css = "div[class='fancybox-inner']")
    private WebElement errorPopUp;

    @FindBy (css = "div[class='fancybox-inner']>p")
    private WebElement errorMessage;

    public ErrorWhishlistPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(Driver.getInstance(), this);
        Driver.manageImplicitWait();
    }

    public ErrorWhishlistPage assertErrorIsDisplayed() {
        log.info("Checking if error message is displayed properly.");
        Asserts.assertIsDisplayed(errorPopUp,
                errorMessage);
        Asserts.assertMessageIsCorrect(errorMessage, ERRORWISHLIST_MESSAGE_ERROR);
        return this;
    }

}
