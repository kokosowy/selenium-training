package com.automationpractice.ProductPages;

import com.automationpractice.commonclasses.Asserts;
import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.webElements.Button;
import com.automationpractice.commonclasses.webElements.Label;
import com.automationpractice.commonclasses.webElements.implementations.decorator.ElementDecorator;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static helper.PathsAndTexts.*;

@Slf4j
public class ComparePage {

    @FindBy(css = "h1[class='page-heading']")
    private Label comparisonSectionLabel;

    @FindBy(css = "td[class='td_empty compare_extra_information']>span")
    private Label featuresLabel;

    @FindBy(css = "ul[class='footer_link']>li>a>span")
    private Button continueShoppingButton;

    @FindBy(xpath = "//*[@id='product_comparison']/tbody/tr[2]/td[1]/strong")
    private Label feature1Label;

    @FindBy(xpath = "//*[@id='product_comparison']/tbody/tr[2]/td[2]")
    private Label feature1Product1Label;

    @FindBy(xpath = "//*[@id='product_comparison']/tbody/tr[2]/td[3]")
    private Label feature1Product2Label;

    @FindBy(xpath = "//*[@id='product_comparison']/tbody/tr[3]/td[1]/strong")
    private Label feature2Label;

    @FindBy(xpath = "//*[@id='product_comparison']/tbody/tr[3]/td[2]")
    private Label feature2Product1Label;

    @FindBy(xpath = "//*[@id='product_comparison']/tbody/tr[3]/td[3]")
    private Label feature2Product2Label;

    @FindBy(xpath = "//*[@id='product_comparison']/tbody/tr[4]/td[1]/strong")
    private Label feature3Label;

    @FindBy(xpath = "//*[@id='product_comparison']/tbody/tr[4]/td[2]")
    private Label feature3Product1Label;

    @FindBy(xpath = "//*[@id='product_comparison']/tbody/tr[4]/td[3]")
    private Label feature3Product2Label;

    public ComparePage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(new ElementDecorator(Driver.getInstance()), this);
        Driver.manageImplicitWait();
    }

    public ComparePage assertComparePageIsDisplayed() {
        log.info("Checking if Compare page is displayed with required elements.");
        Asserts.assertIsDisplayed(comparisonSectionLabel,
                featuresLabel,
                continueShoppingButton);
        return this;
    }

    public ComparePage assertWhatFeaturesAreCompared(int... ints) {
        log.info("Checking filters compared:");
        for (int i : ints) {
            switch (i) {
                case 1:
                    log.info("Compositions - Polyester.");
                    if ((feature1Label.getText()).equals(COMPOSITION_FEATURE)) {
                        Asserts.assertMessageIsCorrect(feature1Product1Label, POLYESTER_COMPOSITION);
                        Asserts.assertMessageIsCorrect(feature1Product2Label, POLYESTER_COMPOSITION);
                    } else if ((feature2Label.getText()).equals(COMPOSITION_FEATURE)) {
                        Asserts.assertMessageIsCorrect(feature2Product1Label, POLYESTER_COMPOSITION);
                        Asserts.assertMessageIsCorrect(feature2Product2Label, POLYESTER_COMPOSITION);
                    } else {
                        Asserts.assertMessageIsCorrect(feature3Product1Label, POLYESTER_COMPOSITION);
                        Asserts.assertMessageIsCorrect(feature3Product2Label, POLYESTER_COMPOSITION);
                    }
                    break;
                case 2:
                    log.info("Styles - Girly.");
                    if ((feature1Label.getText()).equals(STYLE_FEATURE)) {
                        Asserts.assertMessageIsCorrect(feature1Product1Label, GIRLY_STYLE);
                        Asserts.assertMessageIsCorrect(feature1Product2Label, GIRLY_STYLE);
                    } else if ((feature2Label.getText()).equals(STYLE_FEATURE)) {
                        Asserts.assertMessageIsCorrect(feature2Product1Label, GIRLY_STYLE);
                        Asserts.assertMessageIsCorrect(feature2Product2Label, GIRLY_STYLE);
                    } else {
                        Asserts.assertMessageIsCorrect(feature3Product1Label, GIRLY_STYLE);
                        Asserts.assertMessageIsCorrect(feature3Product2Label, GIRLY_STYLE);
                    }
                    break;
            }
        }
        return this;
    }

}
