package com.automationpractice.ProductPages;

import com.automationpractice.commonclasses.Asserts;
import com.automationpractice.commonclasses.AwaitilityWait;
import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Waits;
import com.automationpractice.commonclasses.webElements.Button;
import com.automationpractice.commonclasses.webElements.CheckBox;
import com.automationpractice.commonclasses.webElements.Label;
import com.automationpractice.commonclasses.webElements.implementations.decorator.ElementDecorator;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static helper.PathsAndTexts.*;

@Slf4j
public class CatalogPage {

    @FindBy(css = "p[class='title_block']")
    private Label catalogLabel;

    @FindBy(css = "span[class='cat-name']")
    private Label displayedCategoryLabel;

    @FindBy(css = "button[class='btn btn-default button button-medium bt_compare bt_compare_bottom']")
    private Button compareButton;

    @FindBy(id = "layered_id_feature_1")
    private CheckBox polyesterCheckBox;

    @FindBy(id = "layered_id_feature_13")
    private CheckBox girlyCheckBox;

    @FindBy(id = "layered_quantity_1")
    private CheckBox inStockCheckBox;

    @FindBy(css = "div[id='enabled_filters']>span")
    private Label enabledFiltersLabel;

    @FindBy(css = "span[class='heading-counter']>span")
    private Label filteredProductsCounterLabel;

    @FindBy(css = "li[class='ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line last-line " +
            "first-item-of-tablet-line first-item-of-mobile-line last-mobile-line']>div")
    private WebElement summerDressImage;

    @FindBy(css = "a[class='add_to_compare'][data-id-product='6']")
    private WebElement summerDressAddToCompareButton;

    @FindBy(css = "li[class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-line last-item-of-tablet-line "+
            "last-mobile-line']>div")
    private WebElement chiffonDressImage;

    @FindBy(css = "a[class='add_to_compare'][data-id-product='7']")
    private WebElement chiffonDressAddToCompareButton;

    @FindBy(css = "div[class='bottom-pagination-content clearfix']>form>button>span>strong")
    private Label compareCounterLabel;

    @FindBy(css = "div[class='bottom-pagination-content clearfix']>form")
    private Button bottomCompareButton;

    private Actions actions = new Actions(Driver.getInstance());

    public CatalogPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(new ElementDecorator(Driver.getInstance()), this);
        Driver.manageImplicitWait();
    }

    public CatalogPage assertCatalogPageIsDisplayed() {
        log.info("Checking if Catalog page is displayed with required elements.");
        Asserts.assertIsDisplayed(catalogLabel,
                displayedCategoryLabel,
                compareButton);
        return this;
    }

    public CatalogPage checkGivenFilters(int... ints) {
        log.info("Checking given filters:");
        for (int i : ints) {
            switch (i) {
                case 1:
                    log.info("Compositions - Polyester.");
                    polyesterCheckBox.check();
                    break;
                case 2:
                    log.info("Styles - Girly.");
                    girlyCheckBox.check();
                    break;
                case 3:
                    log.info("Availability - In stock.");
                    inStockCheckBox.check();
                    break;
            }
        }
        return this;
    }

    public CatalogPage assertFiltersAreApplied() {
        log.info("Checking if filters are applied and return 2 products.");
        Waits.waitUntilVisibilityOf(filteredProductsCounterLabel);
        Asserts.assertIsDisplayed(enabledFiltersLabel,
                filteredProductsCounterLabel);
        Asserts.assertMessageIsCorrect(filteredProductsCounterLabel, FILTER_COUNTER_2);
        return this;
    }

    public CatalogPage clickSummerDressAddToCompareButton() {
        log.info("Clicking Add to Compare button for selected product.");
        actions.moveToElement(summerDressImage).perform();
        actions.moveToElement(summerDressAddToCompareButton).click().perform();
        return this;
    }

    public CatalogPage clickChiffonDressAddToCompareButton() {
        log.info("Clicking Add to Compare button for selected product.");
        actions.moveToElement(chiffonDressImage).perform();
        actions.moveToElement(chiffonDressAddToCompareButton).click().perform();
        return this;
    }

    public CatalogPage assertCompareCounter(String value) throws InterruptedException {
        log.info("Checking if quantity of items to compare is correct.");
        AwaitilityWait.doAwait().until(() -> Asserts.assertMessageIsCorrect(compareCounterLabel, value));
        Asserts.assertMessageIsCorrect(compareCounterLabel, value);
        return this;
    }

    public ComparePage clickCompareButton() {
        log.info("Navigating to Compare Page.");
        Waits.waitUntilIsClickable(compareButton);
        compareButton.click();
        return new ComparePage();
    }

}
