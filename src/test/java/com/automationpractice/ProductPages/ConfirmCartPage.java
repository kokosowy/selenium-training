package com.automationpractice.ProductPages;

import com.automationpractice.MainPages.HomePage;
import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Asserts;
import com.automationpractice.commonclasses.Waits;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static helper.PathsAndTexts.*;

@Slf4j
public class ConfirmCartPage {

    @FindBy(css = "h2>i[class='icon-ok']")
    private WebElement okIcon;

    @FindBy(css = "h2")
    private WebElement cartAddConfirmation;

    @FindBy(css = "div[class='button-container']>span>span")
    private WebElement continueButton;

    @FindBy(css = "[class='cross']")
    private WebElement crossButton;

    public ConfirmCartPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(Driver.getInstance(), this);
        Driver.manageImplicitWait();
    }

    public ConfirmCartPage waitUntilOkIconIsVisible() {
        log.info("Waiting until OK icon is visible.");
        Waits.waitUntilVisibilityOf(okIcon);
        return this;
    }

    public HomePage clickContinueShoppingButton() {
        log.info("Clicking button to continue shopping.");
        continueButton.click();
        return new HomePage();
    }

    public HomePage closingConfirmCartWindow() {
        log.info("Closing window with the cross button.");
        crossButton.click();
        return new HomePage();
    }

    public ConfirmCartPage assertConfirmationMessageIsDisplayed() {
        log.info("Checking if confirmation message is displayed properly.");
        Asserts.assertMessageIsCorrect(cartAddConfirmation, CONFIRMCART_MESSAGE_SUCCESS);
        return this;
    }

}
