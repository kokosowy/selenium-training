package com.automationpractice.commonclasses;

import org.awaitility.core.ConditionEvaluationLogger;
import org.awaitility.core.ConditionFactory;
import static org.awaitility.Awaitility.*;
import static java.util.concurrent.TimeUnit.*;

public class AwaitilityWait {

    private static final int MAX_WAIT_TIME = 30;
    private static final int POOLING_RATE = 1;

    public static ConditionFactory doAwait() {
        return with().conditionEvaluationListener(new ConditionEvaluationLogger())
                .await().ignoreException(NullPointerException.class)
                .pollInterval(POOLING_RATE, SECONDS)
                .atMost(MAX_WAIT_TIME, SECONDS);
    }

}