package com.automationpractice.commonclasses;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class Waits {

    private static final int TIME_OUT = 15;
    private static final int POOLING_RATE = 250;

    private static WebDriverWait wait = new WebDriverWait(Driver.getInstance(), TIME_OUT);

    public static void waitUntilVisibilityOf(WebElement element) {
        wait
                .pollingEvery(POOLING_RATE, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitUntilIsClickable(WebElement element) {
        wait
                .pollingEvery(POOLING_RATE, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }

}
