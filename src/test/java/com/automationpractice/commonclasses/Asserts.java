package com.automationpractice.commonclasses;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import org.openqa.selenium.WebElement;

public class Asserts {

    public static void assertMessageIsCorrect(WebElement element, String value) {
        String text = element.getText();
        assertThat(text, is(equalTo(value)));
    }

    public static void assertIsDisplayed(WebElement... webElements) {
        for (WebElement webElement : webElements) {
            assertThat(isVisible(webElement), is(true));
        }
    }

    private static boolean isVisible(WebElement webElement) {
        boolean isDisplayed;
        try {
            isDisplayed = webElement.isDisplayed();
        } catch (Exception exc) {
            isDisplayed = false;
        }
        return isDisplayed;
    }

}
