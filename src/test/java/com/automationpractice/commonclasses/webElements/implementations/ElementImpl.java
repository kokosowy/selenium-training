package com.automationpractice.commonclasses.webElements.implementations;

import com.automationpractice.commonclasses.Driver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ElementImpl implements Element {

    private WebElement element;
    protected WebDriver driver;
    protected By locator;
    private final int TIME_OUT = 15;

    public ElementImpl(WebElement element) {
        this.driver = Driver.getInstance();
        this.element = element;
    }

    public ElementImpl(final By locator) {
        getElement();
        this.locator = locator;
    }

    @Override
    public void click() {
        getElement();
        //waitForLoadUntilClickable();
        element.click();
        Driver.manageImplicitWait();
    }

    @Override
    public void submit() {
        getElement();
        element.submit();
    }

    @Override
    public void sendKeys(CharSequence... keysToSend) {
        getElement();
        element.sendKeys(keysToSend);
    }

    @Override
    public void clear() {
        getElement();
        element.clear();
    }

    @Override
    public String getTagName() {
        getElement();
        return element.getTagName();
    }

    @Override
    public String getAttribute(String var) {
        getElement();
        return element.getAttribute(var);
    }

    @Override
    public boolean isSelected() {
        getElement();
        return element.isSelected();
    }

    @Override
    public boolean isEnabled() {
        getElement();
        return element.isEnabled();
    }

    @Override
    public String getText() {
        getElement();
        return element.getText();
    }

    @Override
    public List<WebElement> findElements(By var) {
        getElement();
        return element.findElements(var);
    }

    @Override
    public WebElement findElement(By var) {
        getElement();
        return element.findElement(var);
    }

    @Override
    public boolean isDisplayed() {
        waitUntilElementIsVisible(TIME_OUT);
        return isVisible();
    }

    @Override
    public Point getLocation() {
        getElement();
        return element.getLocation();
    }

    @Override
    public Dimension getSize() {
        getElement();
        return element.getSize();
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

    @Override
    public String getCssValue(String var) {
        getElement();
        return element.getCssValue(var);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }

    public ElementImpl getElement() {
        this.driver = Driver.getInstance();
        if (this.locator == null) {
            return this;
        } else {
            this.element = driver.findElement(this.locator);
        }
        return this;
    }

    public boolean isVisible() {
        try {
            getElement();
        } catch (NoSuchElementException e) {
            return false;
        }
        boolean isDisplayed;

        try {
            isDisplayed = this.element.isDisplayed();
        } catch (Exception e) {
            isDisplayed = false;
        }
        return isDisplayed;
    }

    public ElementImpl waitForLoadUntilClickable() {
        return waitForLoadUntilClickable(TIME_OUT);
    }

    public ElementImpl waitForLoadUntilClickable(int timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(Driver.getInstance(), timeout);
            wait
                    .pollingEvery(250, TimeUnit.MILLISECONDS)
                    .ignoring(NoSuchElementException.class);
            this.element = wait.until(ExpectedConditions.elementToBeClickable(locator));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    public ElementImpl waitUntilElementIsVisible(int timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(Driver.getInstance(), timeout);
            wait
                    .pollingEvery(250, TimeUnit.MILLISECONDS)
                    .ignoring(NoSuchElementException.class);
            this.element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return this;
    }

}
