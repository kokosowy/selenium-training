package com.automationpractice.commonclasses.webElements.implementations;

import org.openqa.selenium.WebElement;

public interface Element extends WebElement {

}
