package com.automationpractice.commonclasses.webElements.implementations.decorator;

import com.automationpractice.commonclasses.webElements.implementations.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.Annotations;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;

import static java.text.MessageFormat.format;

public class ElementDecorator extends DefaultFieldDecorator {

    public ElementDecorator(final SearchContext searchContext) {
        super(new DefaultElementLocatorFactory(searchContext));
    }

    @Override
    public Object decorate(final ClassLoader loader, final Field field) {
        if (Element.class.isAssignableFrom(field.getType())) {
            return decorateElement(loader, field);
        }
        return super.decorate(loader, field);
    }

    private Object decorateElement(final ClassLoader loader, final Field field) {
        ElementLocator locator = factory.createLocator(field);
        if (locator == null) {
            return null;
        }
        return createElement(field);
    }

    @SuppressWarnings("unchecked")
    private <E extends WebElement> E createElement(Field field) {
        Object pageElement = null;
        try {
            Class<?> className = Class.forName(format("{0}.{1}",
                    "com.automationpractice.commonclasses.webElements",
                    field.getType().getSimpleName()));

            Annotations annotation = new Annotations(field);
            By by = annotation.buildBy();

            pageElement = className.getDeclaredConstructor(By.class).newInstance(by);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return (E) pageElement;
    }

}