package com.automationpractice.commonclasses.webElements;

import com.automationpractice.commonclasses.webElements.implementations.ElementImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.NoSuchElementException;

public class DropDown extends ElementImpl {

    private Select dropdown;

    public DropDown (By locator) {
        super(locator);
    }

    public void select() {
        dropdown = new Select(getElement());
        getElement().click();
    }

    public String[] getOptions() {
        dropdown = new Select(getElement());
        List<WebElement> elementOptions = dropdown.getOptions();
        String[] options = new String[elementOptions.size()];

        for (int i = 0; i < elementOptions.size(); i++) {
            String option = elementOptions.get(i).getText();
            options[i] = option;
        }
        return options;
    }

    public void selectOptionByValue(String optionValue) {
        dropdown = new Select(getElement());
        dropdown.selectByValue(optionValue);
    }

    public void selectOptionByText(String optionText) {
        dropdown = new Select(getElement());
        dropdown.selectByVisibleText(optionText);
    }

    public void selectOptionByIndex(String optionIndex) {
        dropdown = new Select(getElement());
        dropdown.selectByValue(optionIndex);
    }

    public String getTextOfSelectedOption() {
        dropdown = new Select(getElement());
        WebElement selectedOption = dropdown.getFirstSelectedOption();
        String text = null;
        if (selectedOption != null) {
            text = selectedOption.getText();
        }
        if (text != null) {
            text = text.replace("\n", "").replace("\t", "").trim();
        }
        return text;
    }

    @Override
    public boolean isVisible() {
        return isDisplayed();
    }

    @Override
    public boolean isDisplayed() {
        boolean isPresent;

        try {
            dropdown = new Select(getElement());
            getElement();
            isPresent = true;
        } catch (NoSuchElementException | NullPointerException e) {
            isPresent = false;
        }
        return isPresent;
    }

    @Override
    public boolean isEnabled() {
        dropdown = new Select(getElement());
        WebElement element = getElement();
        return element.isEnabled();
    }

}
