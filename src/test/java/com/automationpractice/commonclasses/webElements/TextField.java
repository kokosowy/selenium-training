package com.automationpractice.commonclasses.webElements;

import com.automationpractice.commonclasses.webElements.implementations.ElementImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TextField extends ElementImpl {

    public TextField (WebElement element) {
        super(element);
    }

    public TextField (By locator) {
        super(locator);
    }

    public String getText() {
        return getElement().getAttribute("value");
    }

    public void setText (String text) {
        WebElement element = getElement();
        element.clear();
        element.sendKeys(text);
    }

    public void clearText() {
        getElement().clear();
    }

}
