package com.automationpractice.commonclasses.webElements;

import com.automationpractice.commonclasses.webElements.implementations.ElementImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Image extends ElementImpl {

    public Image (WebElement element) {
        super(element);
    }

    public Image (By locator) {
        super(locator);
    }

}
