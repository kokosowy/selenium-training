package com.automationpractice.commonclasses.webElements;

import com.automationpractice.commonclasses.webElements.implementations.ElementImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Button extends ElementImpl {

    public Button (WebElement element) {
        super(element);
    }

    public Button (By locator) {
        super(locator);
    }

}
