package com.automationpractice.commonclasses.webElements;

import com.automationpractice.commonclasses.webElements.implementations.ElementImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Label extends ElementImpl {

    public Label (WebElement element) {
        super(element);
    }

    public Label (By locator) {
        super(locator);
    }

    @Override
    public String getText() {
        String text = super.getText();

        while (text.endsWith("*") || text.endsWith(" ") || text.endsWith(":"))
            text = text.substring(0, text.length() - 1);
        while (text.startsWith("* ="))
            text = text.substring(4, text.length());
        return text;
    }
}
