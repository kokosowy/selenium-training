package com.automationpractice.commonclasses.webElements;

import com.automationpractice.commonclasses.webElements.implementations.ElementImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RadioButton extends ElementImpl {

    public RadioButton (WebElement element) {
        super(element);
    }

    public RadioButton (By locator) {
        super(locator);
    }

    public void select() {
        getElement().click();
    }
}
