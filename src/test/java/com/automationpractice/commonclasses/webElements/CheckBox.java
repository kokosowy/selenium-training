package com.automationpractice.commonclasses.webElements;

import com.automationpractice.commonclasses.webElements.implementations.ElementImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CheckBox extends ElementImpl {

    public CheckBox (WebElement element) {
        super(element);
    }

    public CheckBox (By locator) {
        super(locator);
    }

    private void toggle() {
        getElement().click();
    }

    public void check() {
        if (!isSelected()) toggle();
    }

    public void uncheck() {
        if (isSelected()) toggle();
    }

}
