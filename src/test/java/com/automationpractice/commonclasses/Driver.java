package com.automationpractice.commonclasses;

import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.*;
import java.util.concurrent.TimeUnit;

public class Driver {

    private static WebDriver driver;
    private static final int WAIT_TIME = 1;
    private static final int EXTENDED_WAIT_TIME = 10;

    public static WebDriver getInstance() {
        return driver;
    }

    public static void setDriver() {
        FirefoxDriverManager.getInstance().setup();
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }

    public static void navigateTo(String baseUrl) {
        driver.get(baseUrl);
        manageImplicitWait();
    }

    public static void switchToFrame(WebElement element) {
        driver.switchTo().frame(element);
    }

    public static void manageImplicitWait() {
        driver.manage().timeouts().implicitlyWait(WAIT_TIME, TimeUnit.SECONDS);
    }

    private static void manageLongerImplicitWait() {
        driver.manage().timeouts().implicitlyWait(EXTENDED_WAIT_TIME, TimeUnit.SECONDS);
    }

    public static void disposeDriver() {
        driver.close();
        driver.quit();
    }

}
