package com.automationpractice.tests;

import com.automationpractice.commonclasses.Driver;
import com.automationpractice.MainPages.HomePage;
import com.automationpractice.FreeAccessPages.LoginPage;
import com.automationpractice.SettingsPages.MyAccountPage;
import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import java.io.IOException;

@Slf4j
public class ProductPagesTests {

    private HomePage homePage;
    private MyAccountPage myAccountPage;
    private LoginPage loginPage;

    @Before
    public void setUpTest() {
        log.info("Setting up Driver.");
        Driver.setDriver();
        loginPage = new LoginPage();
        loginPage.quickLogIn();
        myAccountPage = new MyAccountPage();
        myAccountPage.navigateToHomePage();
        homePage = new HomePage();
        homePage.assertHomePageIsDisplayed();
    }

    @Test
    public void userAddsItemToCart() {
        log.info("Running test...");

        //given
        homePage

        //when
                .clickBlouseAddToCartButton()
                .waitUntilOkIconIsVisible()

                //then
                .assertConfirmationMessageIsDisplayed();
    }

    @Test
    public void userAddsTwoItemsToCartAndRemovesOne() throws InterruptedException {
        log.info("Running test...");

        //given
        homePage

        //when
                .clickBlouseAddToCartButton()
                .waitUntilOkIconIsVisible()

                //then
                .assertConfirmationMessageIsDisplayed()

                //when
                .closingConfirmCartWindow()

                //then
                .assertHomePageIsDisplayed()

                //when
                .clickFadedTShirtAddToCartButton()
                .waitUntilOkIconIsVisible()

                //then
                .assertConfirmationMessageIsDisplayed()

                //when
                .clickContinueShoppingButton()

                //then
                .assertHomePageIsDisplayed()
                .assertQuantityInCart("2")

                //when
                .clickRemoveFirstItemFromCartButton()

                //then
                .assertQuantityInCart("1");
    }

    @Test
    public void userAddsItemToWishlistFromProductPage() {
        log.info("Running test...");

        //given
        homePage

        //when
                .clickFadedTShirtImage()
                .waitUntilProductLabelIsDisplayed()

                //then
                .assertProductPageIsDisplayed()

                //when
                .changeColor()
                .addToWishlist()

                //then
                .assertConfirmationIsDisplayed();
    }

    @Test
    public void userAddsItemToWishlistFromPopUp() {
        log.info("Running test...");

        //given
        homePage

        //when
                .clickFadedTShirtQuickViewButton()
                .switchToProductFrame()
                .waitUntilProductLabelIsDisplayed()

                //then
                .assertProductPageIsDisplayed()

                //when
                .changeColor()
                .addToWishlist()

                //then
                .assertConfirmationIsDisplayed();
    }

    @After
    public void closeTest() throws IOException {
        log.info("Disposing driver after complete test.");
        Driver.disposeDriver();
    }

}
