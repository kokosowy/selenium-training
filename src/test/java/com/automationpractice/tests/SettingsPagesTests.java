package com.automationpractice.tests;

import com.automationpractice.commonclasses.Driver;
import com.automationpractice.FreeAccessPages.LoginPage;
import com.automationpractice.SettingsPages.MyAccountPage;
import com.automationpractice.SettingsPages.PersonalInfoPage;
import helper.UserCredentials;
import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import java.io.IOException;

@Slf4j
public class SettingsPagesTests {

    private LoginPage loginPage;
    private MyAccountPage myAccountPage;
    private PersonalInfoPage personalInfoPage;

    @Before
    public void setUpTest() {
        log.info("Setting up Driver.");
        Driver.setDriver();
        loginPage = new LoginPage();
        loginPage.quickLogIn();
        myAccountPage = new MyAccountPage();
        myAccountPage.navigateToPersonalInfoPage();
    }

    @Test
    public void userUpdatesPersonalInfo() {
        log.info("Running test...");

        //given
        personalInfoPage = new PersonalInfoPage();

        //when
        personalInfoPage
                .waitUntilSaveButtonIsClickable()

                //then
                .assertPersonalInfoPageIsDisplayed()

                //when
                .changeSocialTitle()
                .changeDoBMonth("4")
                .changeNewsletterSetting()
                .confirmCurrentPassword(new UserCredentials().getFromFile(2))
                .clickSaveButton()
                .waitUntilAlertIsDisplayed()

                //then
                .assertSuccessfulAlertIsDisplayed();
    }

    @After
    public void closeTest() throws IOException {
        log.info("Disposing driver after complete test.");
        Driver.disposeDriver();
    }

}
