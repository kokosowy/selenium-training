package com.automationpractice.tests;

import static helper.PathsAndTexts.*;

import com.automationpractice.MainPages.SitemapPage;
import com.automationpractice.commonclasses.Driver;
import com.automationpractice.FreeAccessPages.ContactPage;
import com.automationpractice.FreeAccessPages.LoginPage;
import com.automationpractice.MainPages.HomePage;
import com.automationpractice.SettingsPages.MyAccountPage;
import helper.UserCredentials;
import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import java.io.IOException;

@Slf4j
public class FreeAccessPagesTests {

    private LoginPage loginPage;
    private ContactPage contactPage;
    private HomePage homePage;
    private SitemapPage sitemapPage;

    @Before
    public void setUpTest() {
        log.info("Setting up Driver.");
        Driver.setDriver();
    }

    @Test
    public void userLogsInToMyAccount() {
        log.info("Running test...");

        //given
        loginPage = new LoginPage();

        //when
        loginPage
                .quickLogIn()

        //then
                .assertUserIsLoggedIn(new UserCredentials().getFromFile(3));
    }

    @Test
    public void unauthorizedAttemptsToSendMessage() {
        log.info("Running test...");

        //given
        Driver.navigateTo(BASEURL + CONTACTPAGE);
        contactPage = new ContactPage();
        contactPage

                //then
                .assertContactPageIsDisplayed()

                //when
                .selectHeadingByValue("2")
                .clickSendMessageButton()
                .waitUntilErrorIsDisplayed()

                //then
                .assertValidationErrorIsDisplayed();
    }

    @Test
    public void unauthorizedAttemptsToAddToWishlistViaProductPage() {
        log.info("Running test...");

        //given
        Driver.navigateTo(BASEURL);
        homePage = new HomePage();
        homePage

                //then
                .assertHomePageIsDisplayed()

                //when
                .clickFadedTShirtImage()
                .waitUntilProductLabelIsDisplayed()

                //then
                .assertProductPageIsDisplayed()

                //when
                .addToWishlistWithoutLoggingIn()

                //then
                .assertErrorIsDisplayed();
    }

    @Test
    public void unauthorizedAttemptsToAddToWishlistViaPopUp() {
        log.info("Running test...");

        //given
        Driver.navigateTo(BASEURL);
        homePage = new HomePage();
        homePage

                //then
                .assertHomePageIsDisplayed()

                //when
                .clickFadedTShirtQuickViewButton()
                .switchToProductFrame()
                .waitUntilProductLabelIsDisplayed()

                //then
                .assertProductPageIsDisplayed()

                //when
                .addToWishlistWithoutLoggingIn()

                //then
                .assertErrorIsDisplayed();
    }

    @Test
    public void unauthorizedNavigatesToSitemapAndCatalogPageToCompareProducts() throws InterruptedException {
        log.info("Running test...");

        //given
        Driver.navigateTo(BASEURL + SITEMAPPAGE);
        sitemapPage = new SitemapPage();
        sitemapPage

                //when
                .waitUntilSitemapLabelIsDisplayed()

                //then
                .assertSitemapPageIsDisplayed()

                //when
                .selectDressesCategory()
                .selectSummerDressesSubCategory()

                //then
                .assertCatalogPageIsDisplayed()

                //when
                .checkGivenFilters(1,2,3)

                //then
                .assertFiltersAreApplied()

                //when
                .clickSummerDressAddToCompareButton()

                //then
                .assertCompareCounter("1")

                //when
                .clickChiffonDressAddToCompareButton()

                //then
                .assertCompareCounter("2")

                //when
                .clickCompareButton()

                //then
                .assertComparePageIsDisplayed()
                .assertWhatFeaturesAreCompared(1,2);
    }

    @After
    public void closeTest() throws IOException {
        log.info("Disposing driver after complete test.");
        Driver.disposeDriver();
    }

}
