package com.automationpractice.FreeAccessPages;

import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Asserts;
import com.automationpractice.commonclasses.Waits;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import static helper.PathsAndTexts.*;

@Slf4j
public class ContactPage {

    @FindBy(css="h1[class='page-heading bottom-indent']")
    private WebElement contactPageLabel;

    @FindBy(css="[class='page-subheading']")
    private WebElement sendMessageBox;

    @FindBy(id="id_contact")
    private WebElement selectHeadingOption;

    @FindBy(id="email")
    private WebElement contactEmail;

    @FindBy(id="id_order")
    private WebElement orderNumber;

    @FindBy(css="span[class='action']")
    private WebElement chooseFileButton;

    @FindBy(id="submitMessage")
    private WebElement sendMessageButton;

    @FindBy(css="div[class='alert alert-danger']>p")
    private WebElement errorMessage;

    public ContactPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(Driver.getInstance(), this);
        Driver.manageImplicitWait();
    }

    public ContactPage selectHeadingByValue(String value) {
        log.info("Selecting heading of the message.");
        Select dropdown = new Select(selectHeadingOption);
        dropdown.selectByValue(value);
        return this;
    }

    public ContactPage clickSendMessageButton() {
        log.info("Clicking Send message button.");
        sendMessageButton.click();
        return this;
    }

    public ContactPage assertContactPageIsDisplayed() {
        log.info("Checking if Send Message page is displayed with required elements.");
        Asserts.assertIsDisplayed(contactPageLabel,
                sendMessageBox,
                sendMessageButton);
        return this;
    }

    public ContactPage waitUntilErrorIsDisplayed() {
        log.info("Waiting until error message is visible.");
        Waits.waitUntilVisibilityOf(errorMessage);
        return this;
    }

    public ContactPage assertValidationErrorIsDisplayed() {
        log.info("Checking if validation error is displayed properly.");
        Asserts.assertIsDisplayed(contactPageLabel);
        Asserts.assertMessageIsCorrect(errorMessage, MESSAGE_ERROR);
        return this;
    }

}
