package com.automationpractice.FreeAccessPages;

import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Asserts;
import com.automationpractice.MainPages.HomePage;
import com.automationpractice.SettingsPages.MyAccountPage;
import helper.UserCredentials;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static helper.PathsAndTexts.*;

@Slf4j
public class LoginPage {

    @FindBy(css = "[id='login_form']>h3")
    private WebElement loginBox;

    @FindBy(id = "email")
    private WebElement username;

    @FindBy(id = "passwd")
    private WebElement password;

    @FindBy(id = "SubmitLogin")
    private WebElement loginButton;

    public LoginPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(Driver.getInstance(), this);
        Driver.manageImplicitWait();
    }

    private LoginPage enterLogin (String user) {
        log.info("Providing username.");
        username.clear();
        username.sendKeys(user);
        return this;
    }

    private LoginPage enterPassword (String pass) {
        log.info("Providing password.");
        password.clear();
        password.sendKeys(pass);
        return this;
    }

    private HomePage clickLoginButton() {
        log.info("Clicking Login button.");
        loginButton.click();
        return new HomePage();
    }

    private LoginPage assertLoginPageIsDisplayed() {
        log.info("Checking if Login page is displayed with required elements.");
        Asserts.assertIsDisplayed(loginBox,
                username,
                password,
                loginButton);
        return this;
    }

    public MyAccountPage quickLogIn() {
        log.info("Navigating to Login page.");
        Driver.navigateTo(BASEURL + LOGINPAGE);

        assertLoginPageIsDisplayed();
        enterLogin(new UserCredentials().getFromFile(1));
        enterPassword(new UserCredentials().getFromFile(2));
        clickLoginButton();
        new MyAccountPage().waitUntilVisibilityOfUsernameLabel();
        return new MyAccountPage();
    }

}
