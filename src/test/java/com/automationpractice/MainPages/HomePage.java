package com.automationpractice.MainPages;

import com.automationpractice.commonclasses.AwaitilityWait;
import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Asserts;
import com.automationpractice.ProductPages.ConfirmCartPage;
import com.automationpractice.ProductPages.ProductPage;
import com.automationpractice.commonclasses.Waits;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Slf4j
public class HomePage {

    @FindBy(id = "header_logo")
    private WebElement logoImage;

    @FindBy(css = "[class='shopping_cart']>a")
    private WebElement cartButton;

    @FindBy(css = "a[title='Women']")
    private WebElement womenCategoryButton;

    @FindBy(css = "[class='homefeatured']")
    private WebElement popularSubCategoryButton;

    @FindBy(css = "a[class='blockbestsellers']")
    private WebElement bestSellersSubCategoryButton;

    @FindBy(id = "homefeatured")
    private WebElement productListGrid;

    @FindBy(css = "[id='homefeatured']>li[class='ajax_block_product col-xs-12 col-sm-4 col-md-3 " +
            "last-item-of-mobile-line']>div")
    private WebElement blouseImage;

    @FindBy(xpath = "//*[@id='homefeatured']/li[2]/div/div[2]/div[2]/a[1]")
    private WebElement blouseAddToCartButton;

    @FindBy(css = "[id='homefeatured']>li[class='ajax_block_product col-xs-12 col-sm-4 col-md-3 first-in-line " +
            "first-item-of-tablet-line first-item-of-mobile-line']>div")
    private WebElement fadedTShirtImage;

    @FindBy(xpath = "//*[@id='homefeatured']/li[1]/div/div[2]/div[2]/a[1]")
    private WebElement fadedTShirtAddToCartButton;

    @FindBy(xpath = "//*[@id='homefeatured']/li[1]/div/div[1]/div/a[2]")
    private WebElement fadedTShirtQuickViewButton;

    @FindBy(xpath = "//*[@id='header']/div[3]/div/div/div[3]/div/a/span[1]")
    private WebElement quantityInCartLabel;

    @FindBy(css = "dt[class='first_item']>span>a")
    private WebElement removeFirstItemFromCartButton;

    @FindBy(css = "a[title='Sitemap']")
    private WebElement sitemapLink;

    private Actions actions = new Actions(Driver.getInstance());

    public HomePage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(Driver.getInstance(), this);
        Driver.manageImplicitWait();
    }

    public HomePage changeToBestSellers() {
        log.info("Changing subcategory to Best Sellers.");
        bestSellersSubCategoryButton.click();
        return this;
    }

    public HomePage assertHomePageIsDisplayed() {
        log.info("Checking if Home page is displayed with required elements.");
        Asserts.assertIsDisplayed(logoImage,
                productListGrid,
                cartButton,
                womenCategoryButton,
                popularSubCategoryButton,
                bestSellersSubCategoryButton);
        return this;
    }

    public ProductPage clickFadedTShirtImage() {
        log.info("Clicking image of selected product.");
        fadedTShirtImage.click();
        return new ProductPage();
    }

    public ProductPage clickFadedTShirtQuickViewButton() {
        log.info("Clicking Quick View button for selected product.");
        actions.moveToElement(fadedTShirtImage).perform();
        actions.moveToElement(fadedTShirtQuickViewButton).click().perform();
        return new ProductPage();
    }

    public ConfirmCartPage clickBlouseAddToCartButton() {
        log.info("Clicking Add To Cart button for selected product.");
        actions.moveToElement(blouseImage).perform();
        actions.moveToElement(blouseAddToCartButton).click().perform();
        return new ConfirmCartPage();
    }

    public ConfirmCartPage clickFadedTShirtAddToCartButton() {
        log.info("Clicking Add To Cart button for selected product.");
        actions.moveToElement(fadedTShirtImage).perform();
        actions.moveToElement(fadedTShirtAddToCartButton).click().perform();
        return new ConfirmCartPage();
    }

    public HomePage assertQuantityInCart(String value) throws InterruptedException {
        log.info("Checking if quantity of items in cart is correct.");
        AwaitilityWait.doAwait().until(() -> Asserts.assertMessageIsCorrect(quantityInCartLabel, value));
        Asserts.assertMessageIsCorrect(quantityInCartLabel, value);
        return this;
    }

    public HomePage clickRemoveFirstItemFromCartButton() {
        log.info("Clicking button to remove first item from the cart.");
        actions.moveToElement(quantityInCartLabel).perform();
        Waits.waitUntilVisibilityOf(removeFirstItemFromCartButton);
        actions.moveToElement(removeFirstItemFromCartButton).click().perform();
        return this;
    }

    public SitemapPage clickSitemapLink() {
        log.info("Clicking on Sitemap link.");
        sitemapLink.click();
        return new SitemapPage();
    }
}