package com.automationpractice.MainPages;

import com.automationpractice.ProductPages.CatalogPage;
import com.automationpractice.commonclasses.Asserts;
import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.webElements.Button;
import com.automationpractice.commonclasses.webElements.Label;
import com.automationpractice.commonclasses.webElements.implementations.decorator.ElementDecorator;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.automationpractice.commonclasses.Waits.*;

@Slf4j
public class SitemapPage {

    @FindBy(css = "h1[class='page-heading']")
    private Label sitemapLabel;

    @FindBy(css = "span[class='navigation_page']")
    private Label navigationLabel;

    @FindBy(css = "div[id='categories_block_left']>h2")
    private Label categoriesLabel;

    @FindBy(css = "div[class='block_content']>ul>li>span")
    private Button womenCategoryTreeExpandButton;

    @FindBy(css = "div[class='block_content']>ul>li>ul>li[class='last']>span")
    private Button dressesCategoryTreeExpandButton;

    @FindBy(css = "div[class='block_content']>ul>li>ul>li[class='last']>ul>li[class='last']>a")
    private Button summerDressesCategoryTreeExpandButton;

    public SitemapPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(new ElementDecorator(Driver.getInstance()), this);
        Driver.manageImplicitWait();
    }

    public SitemapPage waitUntilSitemapLabelIsDisplayed() {
        log.info("Wait until Sitemap label is visible.");
        waitUntilVisibilityOf(sitemapLabel);
        return this;
    }

    public SitemapPage assertSitemapPageIsDisplayed() {
        log.info("Checking if Sitemap page is displayed with required elements.");
        Asserts.assertIsDisplayed(navigationLabel,
                sitemapLabel,
                categoriesLabel);
        return this;
    }

    public SitemapPage selectDressesCategory() {
        log.info("Expanding category tree and selecting dresses category.");
        womenCategoryTreeExpandButton.click();
        waitUntilIsClickable(dressesCategoryTreeExpandButton);
        dressesCategoryTreeExpandButton.click();
        return this;
    }

    public CatalogPage selectSummerDressesSubCategory() {
        log.info("Expanding category tree further and selecting summer dresses subcategory.");
        waitUntilIsClickable(summerDressesCategoryTreeExpandButton);
        summerDressesCategoryTreeExpandButton.click();
        return new CatalogPage();
    }

}
