package com.automationpractice.SettingsPages;

import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Asserts;
import com.automationpractice.MainPages.HomePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static com.automationpractice.commonclasses.Waits.*;

@Slf4j
public class MyAccountPage {

    @FindBy(css = "i[class='icon-user']")
    private WebElement userIcon;

    @FindBy(css = "h1[class='page-heading']")
    private WebElement myAccountBox;

    @FindBy(css = "a[title='Home']")
    private WebElement homeButton;

    @FindBy(css = "[title='Information']>span")
    private WebElement myPersonalInfoButton;

    @FindBy(css = ".account>span")
    private WebElement usernameLabel;

    @FindBy(css = ".logout")
    private WebElement logoutButton;

    public MyAccountPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(Driver.getInstance(), this);
        Driver.manageImplicitWait();
    }

    private MyAccountPage assertMyAccountPageIsDisplayed() {
        log.info("Checking if My Account page is displayed with required elements.");
        Asserts.assertIsDisplayed(userIcon,
                myAccountBox,
                homeButton,
                myPersonalInfoButton);
        return this;
    }

    private MyAccountPage waitUntilUserIconIsClickable() {
        log.info("Waiting until user icon is clickable.");
        waitUntilIsClickable(userIcon);
        return this;
    }

    private PersonalInfoPage clickUserIcon() {
        log.info("Clicking user icon.");
        userIcon.click();
        return new PersonalInfoPage();
    }

    private MyAccountPage waitUntilHomeButtonIsClickable() {
        log.info("Waiting until user icon is clickable.");
        waitUntilIsClickable(homeButton);
        return this;
    }

    private HomePage clickHomeButton() {
        log.info("Clicking Home button.");
        homeButton.click();
        return new HomePage();
    }

    public HomePage navigateToHomePage() {
        assertMyAccountPageIsDisplayed();
        waitUntilHomeButtonIsClickable();
        clickHomeButton();
        return new HomePage();
    }

    public PersonalInfoPage navigateToPersonalInfoPage() {
        assertMyAccountPageIsDisplayed();
        waitUntilUserIconIsClickable();
        clickUserIcon();
        return new PersonalInfoPage();
    }

    public MyAccountPage assertUserIsLoggedIn(String username) {
        log.info("Checking if user is logged in.");
        Asserts.assertIsDisplayed(logoutButton);
        Asserts.assertMessageIsCorrect(usernameLabel, username);
        return this;
    }

    public MyAccountPage waitUntilVisibilityOfUsernameLabel() {
        log.info("Waiting until username label is visible.");
        waitUntilVisibilityOf(usernameLabel);
        return this;
    }

}
