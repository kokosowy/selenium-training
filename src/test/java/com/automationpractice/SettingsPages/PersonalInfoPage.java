package com.automationpractice.SettingsPages;

import com.automationpractice.commonclasses.Driver;
import com.automationpractice.commonclasses.Asserts;
import com.automationpractice.commonclasses.Waits;
import com.automationpractice.commonclasses.webElements.*;
import com.automationpractice.commonclasses.webElements.implementations.decorator.ElementDecorator;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static helper.PathsAndTexts.*;

@Slf4j
public class PersonalInfoPage {

    @FindBy(css="h1[class='page-subheading']")
    private Label personalInfoTitle;

    @FindBy(id="id_gender1")
    private RadioButton mrRadioButton;

    @FindBy(id="id_gender2")
    private RadioButton mrsRadioButton;

    @FindBy(id="old_passwd")
    private TextField currentPassword;

    @FindBy(id="months")
    private DropDown monthDropDown;

    @FindBy(id="newsletter")
    private CheckBox newsletterCheckBox;

    @FindBy(id="optin")
    private CheckBox specialOffersCheckBox;

    @FindBy(css="button[name='submitIdentity']")
    private Button saveButton;

    @FindBy(css="p[class='alert alert-success']")
    private Label successfulUpdateAlert;

    public PersonalInfoPage() {
        Driver.manageImplicitWait();
        PageFactory.initElements(new ElementDecorator(Driver.getInstance()), this);
        Driver.manageImplicitWait();
    }

    public PersonalInfoPage assertPersonalInfoPageIsDisplayed() {
        log.info("Checking if Personal Information page is displayed with required elements.");
        Asserts.assertIsDisplayed(personalInfoTitle,
                saveButton);
        return this;
    }

    public PersonalInfoPage changeDoBMonth(String optionValue) {
        log.info("Changing month of Date of Birth.");
        monthDropDown.selectOptionByValue(optionValue);
        return this;
    }

    public PersonalInfoPage changeSocialTitle() {
        log.info("Changing social title.");
        if (mrRadioButton.isSelected()) {
            mrsRadioButton.select();
        } else mrRadioButton.select();
        return this;
    }

    public PersonalInfoPage changeNewsletterSetting() {
        log.info("Changing newsletter settings.");
        newsletterCheckBox.check();
        specialOffersCheckBox.check();
        return this;
    }

    public PersonalInfoPage confirmCurrentPassword (String pass) {
        log.info("Providing password to confirm changes.");
        currentPassword.clear();
        currentPassword.sendKeys(pass);
        return this;
    }

    public PersonalInfoPage clickSaveButton() {
        log.info("Clicking Save button.");
        saveButton.click();
        return this;
    }

    public PersonalInfoPage waitUntilSaveButtonIsClickable() {
        log.info("Waiting for Save button to be clickable.");
        Waits.waitUntilIsClickable(saveButton);
        return this;
    }

    public PersonalInfoPage waitUntilAlertIsDisplayed() {
        log.info("Waiting until confirmation message is displayed.");
        Waits.waitUntilVisibilityOf(successfulUpdateAlert);
        return this;
    }

    public PersonalInfoPage assertSuccessfulAlertIsDisplayed() {
        log.info("Checking if confirmation message is displayed with required elements.");
        Asserts.assertMessageIsCorrect(successfulUpdateAlert, PERSONALINFO_MESSAGE_SUCCESS);
        return this;
    }

}
