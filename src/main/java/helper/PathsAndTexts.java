package helper;

public class PathsAndTexts {

    //URLs
    public static final String BASEURL = "http://automationpractice.com/index.php";
    public static final String LOGINPAGE = "?controller=authentication";
    public static final String CONTACTPAGE = "?controller=contact";
    public static final String SITEMAPPAGE = "?controller=sitemap";

    //Paths to files
    static final String FILE_EMAIL = "src\\main\\resources\\email.txt";
    static final String FILE_USERNAME = "src\\main\\resources\\name.txt";
    static final String FILE_PASSWORD = "src\\main\\resources\\password.txt";

    //Confirm Add To Cart Page texts
    public static final String CONFIRMCART_MESSAGE_SUCCESS = "Product successfully added to your shopping cart";

    //Add to Wishlist Confirmation Page texts
    public static final String CONFIRMWISHLIST_MESSAGE_SUCCESS = "Added to your wishlist.";

    //Add to Wishlist Error Page texts
    public static final String ERRORWISHLIST_MESSAGE_ERROR = "You must be logged in to manage your wishlist.";

    //Personal Info Page texts
    public static final String PERSONALINFO_MESSAGE_SUCCESS = "Your personal information has been successfully updated.";

    //Contact Page texts
    public static final String MESSAGE_ERROR = "There is 1 error";

    //Catalog Page texts
    public static final String FILTER_COUNTER_2 = "There are 2 products.";

    //Compare Page texts
    public static final String COMPOSITION_FEATURE = "Compositions";
    public static final String STYLE_FEATURE = "Styles";
    public static final String POLYESTER_COMPOSITION = "Polyester";
    public static final String GIRLY_STYLE = "Girly";

}
