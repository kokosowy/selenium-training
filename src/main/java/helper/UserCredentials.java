package helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static helper.PathsAndTexts.*;

public class UserCredentials {

    public String getFromFile(int typeOfData) {
        String output = "";
        String pathToFile = "";
        switch (typeOfData) {
            case 1: //email
                pathToFile = FILE_EMAIL;
                break;
            case 2: //password
                pathToFile = FILE_PASSWORD;
                break;
            case 3: //name
                pathToFile = FILE_USERNAME;
                break;
            default:
                System.out.println("Incorrect type of data.");
        }

        try {
            BufferedReader inputFile = new BufferedReader(new FileReader(pathToFile));
            output = inputFile.readLine();
            inputFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return output;
    }

}
